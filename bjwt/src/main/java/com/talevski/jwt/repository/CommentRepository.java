package com.talevski.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talevski.jwt.model.domain.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{

}
