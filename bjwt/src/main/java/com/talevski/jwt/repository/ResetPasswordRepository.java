package com.talevski.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.talevski.jwt.model.ResetPasswordObject;

public interface  ResetPasswordRepository extends JpaRepository<ResetPasswordObject, Integer>{

	ResetPasswordObject findByToken(String token);
	
	
}
