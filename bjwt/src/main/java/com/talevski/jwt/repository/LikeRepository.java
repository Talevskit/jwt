package com.talevski.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talevski.jwt.model.domain.Like;

@Repository
public interface LikeRepository extends JpaRepository<Like, Integer>{

}
