package com.talevski.jwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.talevski.jwt.model.domain.Post;
@Repository
public interface PostRepository extends JpaRepository<Post, Integer>, CrudRepository<Post, Integer>{

}
