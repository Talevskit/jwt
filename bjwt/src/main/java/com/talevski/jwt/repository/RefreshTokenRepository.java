package com.talevski.jwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.talevski.jwt.model.RefreshToken;
import com.talevski.jwt.model.User;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer>{

	Optional<RefreshToken> findByToken(String token);
	Integer deleteByUser(User user);
}
