package com.talevski.jwt.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenEmailObj {

	private String token;
	private String email;
	
}
