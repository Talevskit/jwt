package com.talevski.jwt.model.domain;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.talevski.jwt.infrastructure.domain.BasePost;
import lombok.Data;

@Data
@Entity
@Table(name = "posts")
public class Post extends BasePost{
	
	private String title;
	private String description;
//	private Integer likes = null;
//	private Integer dislikes = null;
	@OneToMany(mappedBy="post", cascade=CascadeType.ALL)
	private List<Comment> comments;
	@OneToMany(mappedBy="post", cascade=CascadeType.ALL)
	private List<Like> likes;
	@OneToMany(mappedBy="post", cascade=CascadeType.ALL)
	private List<Dislike> dislikes;
	

}
