package com.talevski.jwt.model.mapper;

import com.talevski.jwt.infrastructure.mapper.GeneralMapper;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.dto.PostDto;

public interface PostMapper extends GeneralMapper<PostDto, Post> {
		public void mapRequestedFieldForUpdate(PostDto postDto, Post post);
	}
