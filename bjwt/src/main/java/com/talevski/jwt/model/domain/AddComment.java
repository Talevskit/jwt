package com.talevski.jwt.model.domain;

import lombok.Data;

@Data
public class AddComment {
	private Integer id;
	private Comment comment;

}
