package com.talevski.jwt.model.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {
	
	private Integer id;
	private String username;
	private String email;

}
