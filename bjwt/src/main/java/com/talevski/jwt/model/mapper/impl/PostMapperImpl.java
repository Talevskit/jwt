package com.talevski.jwt.model.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.talevski.jwt.infrastructure.mapper.AbstractGeneralMapper;
import com.talevski.jwt.model.domain.Comment;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.dto.PostDto;
import com.talevski.jwt.model.mapper.PostMapper;

@Component
public class PostMapperImpl extends AbstractGeneralMapper implements PostMapper {

	public PostMapperImpl(ModelMapper modelMapper) {
		super(modelMapper);
	}

	@Override
	public PostDto entityToDto(Post post) {
		return this.modelMapper.map(post, PostDto.class);

	}

	@Override
	public Post dtoToEntity(PostDto postDto) {
		return this.modelMapper.map(postDto, Post.class);
	}

	@Override
	public void mapRequestedFieldForUpdate(PostDto postDto, Post post) {
		post.setTitle(postDto.getTitle());
		post.setDescription(postDto.getDescription());
		postDto.getLikes().addAll(post.getLikes());
		postDto.getDislikes().addAll(post.getDislikes());
		postDto.getComments().addAll(post.getComments());
	}

}
