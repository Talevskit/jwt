package com.talevski.jwt.model.dto;

import java.util.List;

import com.talevski.jwt.infrastructure.dto.BasePostDto;
import com.talevski.jwt.model.domain.Comment;
import com.talevski.jwt.model.domain.Dislike;
import com.talevski.jwt.model.domain.Like;

import lombok.Data;

@Data
public class PostDto extends BasePostDto{
	private String title;
	private String description;
	private List<Like> likes;
	private List<Dislike> dislikes;
	private List<Comment> comments;
}
