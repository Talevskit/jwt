package com.talevski.jwt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sendgrid.Response;
import com.talevski.jwt.infrastructure.Endpoints;
import com.talevski.jwt.model.EmailObj;
import com.talevski.jwt.model.PasswordObj;
import com.talevski.jwt.payload.request.EmailRequest;
import com.talevski.jwt.service.impl.EmailServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(Endpoints.AUTH)
public class EmailController {
	
	@Autowired
	private EmailServiceImpl emailServiceImpl;
	
	@PostMapping("/sendEmail")
	public ResponseEntity<String> sendEmail(@RequestBody EmailRequest emailRequest){
		Response response = emailServiceImpl.sendEmail(emailRequest);
		if(response.getStatusCode() == 200 || response.getStatusCode() == 202)
			return new ResponseEntity<>("Email sent succesfully", HttpStatus.OK);
		return new ResponseEntity<>("Failed to send", HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/resetPasswordRequest")
	public String resetPassword(@RequestBody EmailObj emailObj) {
		 return emailServiceImpl.resetPassword(emailObj);
	}

	@PostMapping("/resetPassword")
	public String setNewPassword(@RequestBody PasswordObj passwordObj,@RequestParam("token") String token) throws Exception {
		return emailServiceImpl.setNewPassword(token, passwordObj.getPassword());
	}
	

}
