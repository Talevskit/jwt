package com.talevski.jwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talevski.jwt.infrastructure.Endpoints;
import com.talevski.jwt.model.User;
import com.talevski.jwt.service.impl.UserServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@GetMapping
	public List<User> getAll() {
		return userServiceImpl.getAll();
	}
}
