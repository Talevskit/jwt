package com.talevski.jwt.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.talevski.jwt.infrastructure.Endpoints;
import com.talevski.jwt.payload.request.LoginRequest;
import com.talevski.jwt.payload.request.LogoutRequest;
import com.talevski.jwt.payload.request.RefreshTokenRequest;
import com.talevski.jwt.payload.request.SignupRequest;
import com.talevski.jwt.service.impl.AuthServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(Endpoints.AUTH)
public class AuthController {

	@Autowired
	private AuthServiceImpl authServiceImpl;
	
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		return authServiceImpl.authenticateUser(loginRequest);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		return authServiceImpl.registerUser(signUpRequest);
	}
	
	@PostMapping("/refreshtoken")
	public ResponseEntity<?> refreshtoken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
		return authServiceImpl.refreshToken(refreshTokenRequest);
	}
	
	@PostMapping("/logout")
	public ResponseEntity<?> logout(@Valid @RequestBody LogoutRequest logoutRequest) {
		return authServiceImpl.logoutUser(logoutRequest);
	}
}
