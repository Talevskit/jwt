package com.talevski.jwt.controller;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.talevski.jwt.infrastructure.Endpoints;
import com.talevski.jwt.model.domain.AddComment;
import com.talevski.jwt.model.domain.Comment;
import com.talevski.jwt.model.domain.Dislike;
import com.talevski.jwt.model.domain.Like;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.domain.UserDto;
import com.talevski.jwt.model.dto.PostDto;
import com.talevski.jwt.service.impl.CommentServiceImpl;
import com.talevski.jwt.service.impl.DislikeServiceImpl;
import com.talevski.jwt.service.impl.LikeServiceImpl;
import com.talevski.jwt.service.impl.PostServiceImpl;

@RequestMapping(Endpoints.POSTS)
@RestController
@CrossOrigin
public class PostController {

	@Autowired
	private PostServiceImpl postServiceImpl;
	@Autowired
	private CommentServiceImpl commentServiceImpl;
	@Autowired
	private LikeServiceImpl likeServiceImpl;
	@Autowired
	private DislikeServiceImpl dislikeServiceImpl;
	
	@GetMapping("/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public PostDto getById(@PathVariable("id") Integer id) {
		return postServiceImpl.findById(id);
	}
	
	@GetMapping
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<PostDto> getAll(){
		return postServiceImpl.getAll();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@PreAuthorize("hasRole('ADMIN')")
	public PostDto Post(@RequestBody PostDto postDto) {
		List<Like> likes = Collections.emptyList();
		List<Dislike> dislikes = Collections.emptyList();
		List<Comment> comments = Collections.emptyList();
			postDto.setLikes(likes);
			postDto.setDislikes(dislikes);
			postDto.setComments(comments);
		return postServiceImpl.createPost(postDto);
		
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	@PreAuthorize("hasRole('USER')")
	public PostDto updatePost(@PathVariable("id") Integer id, @RequestBody PostDto postDto) {
		return postServiceImpl.updatePost(id, postDto);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasRole('MODERATOR')")
	public void removePost(@PathVariable("id") Integer id) {
		postServiceImpl.removePost(id);
	}
	
	@DeleteMapping("/deleteComment/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public void removeComment(@PathVariable("id") Integer id) {
		commentServiceImpl.removeComment(id);
	}
	
	@PutMapping("/addComment")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public Comment addComment(@RequestBody AddComment addComment) {
		return commentServiceImpl.addComment(addComment);
	}
	
	@PostMapping("/like/{id}")
	@PreAuthorize("hasRole('USER')")
	public Like likeIt(@PathVariable("id") Integer id, @RequestBody Like like) {
		return likeServiceImpl.likeIt(id, like);
	}
	
	@PostMapping("/dislike/{id}")
	@PreAuthorize("hasRole('USER')")
	public Dislike dislikeIt(@PathVariable("id") Integer id, @RequestBody Dislike dislike) {
		return dislikeServiceImpl.dislikeIt(id, dislike);
	}
	
	@GetMapping("/getLikes/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<UserDto> likeUsers(@PathVariable("id") Integer id) {
		return likeServiceImpl.likeUsers(id);
	}
	
	@GetMapping("/getDislikes/{id}")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public List<UserDto> dislikeUsers(@PathVariable("id") Integer id) {
		return dislikeServiceImpl.dislikeUsers(id);
	}

}
