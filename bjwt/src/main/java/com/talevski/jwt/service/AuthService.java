package com.talevski.jwt.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.talevski.jwt.payload.request.LoginRequest;
import com.talevski.jwt.payload.request.LogoutRequest;
import com.talevski.jwt.payload.request.RefreshTokenRequest;
import com.talevski.jwt.payload.request.SignupRequest;

@Service
public interface AuthService {
	
	ResponseEntity<?> authenticateUser(LoginRequest loginRequest);
	ResponseEntity<?> registerUser(SignupRequest signUpRequest);
	ResponseEntity<?> refreshToken(RefreshTokenRequest refreshTokenRequest);
	ResponseEntity<?> logoutUser(LogoutRequest logoutRequest);
	
}
