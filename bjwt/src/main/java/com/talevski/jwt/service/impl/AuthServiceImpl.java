package com.talevski.jwt.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.ERole;
import com.talevski.jwt.model.RefreshToken;
import com.talevski.jwt.model.Role;
import com.talevski.jwt.model.User;
import com.talevski.jwt.payload.request.LoginRequest;
import com.talevski.jwt.payload.request.LogoutRequest;
import com.talevski.jwt.payload.request.RefreshTokenRequest;
import com.talevski.jwt.payload.request.SignupRequest;
import com.talevski.jwt.payload.response.JwtResponse;
import com.talevski.jwt.payload.response.MessageResponse;
import com.talevski.jwt.payload.response.RefreshTokenResponse;
import com.talevski.jwt.repository.RoleRepository;
import com.talevski.jwt.repository.UserRepository;
import com.talevski.jwt.security.jwt.JwtUtils;
import com.talevski.jwt.security.services.UserDetailsImpl;
import com.talevski.jwt.service.AuthService;
@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private JwtUtils jwtUtils;
	
	@Autowired
	private RefreshTokenServiceImpl refreshTokenServiceImpl;

	@Override
	public ResponseEntity<?> authenticateUser(LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		String jwt = jwtUtils.generateToken(authentication);
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
		
	    RefreshToken refreshToken = refreshTokenServiceImpl.createRefreshToken(userDetails.getId());


		return ResponseEntity.ok(
				new JwtResponse(userDetails.getId(), jwt, refreshToken.getToken(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@Override
	public ResponseEntity<?> registerUser(SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@Override
	public ResponseEntity<?> refreshToken(RefreshTokenRequest refreshTokenRequest) {
		String request = refreshTokenRequest.getRefreshToken();
		return refreshTokenServiceImpl.findByToken(request)
				.map(refreshTokenServiceImpl::verifyExpiration)
        .map(RefreshToken::getUser)
        .map(user -> {
          String token = jwtUtils.getTokenFromUsername(user.getUsername());
          return ResponseEntity.ok(new RefreshTokenResponse(token, request));
        })
        .orElseThrow(() -> new ApiException("Refresh token is not in database!"));
		
	}

	@Override
	public ResponseEntity<?> logoutUser(LogoutRequest logoutRequest) {
		refreshTokenServiceImpl.deleteByUserId(logoutRequest.getUserId());
		return ResponseEntity.ok(new MessageResponse("Logged out succesfully"));
	}

}
