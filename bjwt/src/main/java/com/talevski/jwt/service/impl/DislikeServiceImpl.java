package com.talevski.jwt.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.User;
import com.talevski.jwt.model.domain.Dislike;
import com.talevski.jwt.model.domain.Like;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.domain.UserDto;
import com.talevski.jwt.model.dto.PostDto;
import com.talevski.jwt.model.mapper.PostMapper;
import com.talevski.jwt.repository.DislikeRepository;
import com.talevski.jwt.repository.PostRepository;
import com.talevski.jwt.repository.UserRepository;
import com.talevski.jwt.service.DislikeService;
@Service
public class DislikeServiceImpl implements DislikeService{
	
	@Autowired
	private DislikeRepository dislikeRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private PostMapper postMapper;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public Dislike findById(Integer id) {
		return dislikeRepository.findById(id).orElseThrow(()-> {
			return new ApiException("not found");
		});
	}

	@Override
	public Dislike dislikeIt(Integer id, Dislike entity) {
		Post persistedPost = postRepository.findById(id).orElseThrow(()-> {
			return new ApiException("Post not found");
		});
		persistedPost.getDislikes().stream().forEach((x) -> {
			if(x.getUserId().equals(entity.getUserId()))
				throw new ApiException("You cant dislike the same post twice..");
		});
		Dislike dislike = new Dislike();
		dislike.setPost(persistedPost);
		dislike.setUserId(entity.getUserId());
		PostDto transientPost = postMapper.entityToDto(persistedPost);
		transientPost.getDislikes().add(dislike);
		postMapper.mapRequestedFieldForUpdate(transientPost, persistedPost);
		postRepository.saveAndFlush(persistedPost);
		dislikeRepository.save(dislike);
		return dislike;
	}

	@Override
	public List<Dislike> getAll() {
		return dislikeRepository.findAll();
	}

	@Override
	public void removeDislike(Integer id) {
		dislikeRepository.deleteById(id);
		
	}

	@Override
	public List<UserDto> dislikeUsers(Integer id) {
		Post post = postRepository.findById(id).orElseThrow(()-> {
			return new ApiException("Post not found");
		});
		List<UserDto> users = new ArrayList<UserDto>();
		post.getDislikes().stream().forEach(x -> {
			User user = userRepository.findById((long) x.getUserId()).get();
			UserDto userDto = new UserDto();
			userDto.setId(user.getId().intValue());
			userDto.setUsername(user.getUsername());
			userDto.setEmail(user.getEmail());
			users.add(userDto);
		});
		
		return users;
	}

}
