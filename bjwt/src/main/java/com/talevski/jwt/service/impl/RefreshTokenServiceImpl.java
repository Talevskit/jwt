package com.talevski.jwt.service.impl;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.RefreshToken;
import com.talevski.jwt.repository.RefreshTokenRepository;
import com.talevski.jwt.repository.UserRepository;
import com.talevski.jwt.service.RefreshTokenService;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
	
	@Value("${talevski.app.jwtRefreshExpirationMs}")
	private Long refreshTokenDuration;
	
	@Autowired
	private RefreshTokenRepository refreshTokenRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Optional<RefreshToken> findByToken(String token) {
		return refreshTokenRepository.findByToken(token);
	}

	@Override
	public RefreshToken createRefreshToken(Long id) {
		RefreshToken refreshToken = new RefreshToken();
		refreshToken.setUser(userRepository.findById(id).orElseThrow(() -> {
			return new ApiException("User not found");
		}));
		refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDuration));
		refreshToken.setToken(UUID.randomUUID().toString());
		return refreshTokenRepository.save(refreshToken);
	}

	@Override
	public RefreshToken verifyExpiration(RefreshToken token) {
		if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
		      refreshTokenRepository.delete(token);
		      throw new ApiException("Token expired. Please signin again.");
		    }

		    return token;
	}

	@Override
	public Boolean deleteByUserId(Long id) {
		refreshTokenRepository.deleteByUser(userRepository.findById(id).get());
		return true;
	}

}
