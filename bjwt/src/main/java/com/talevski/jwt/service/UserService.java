package com.talevski.jwt.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.User;


@Service
public interface UserService {
	
	public User findById(Long id);

	public List<User> getAll();
	
	public Boolean removeUser(Long id);

}
