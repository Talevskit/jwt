package com.talevski.jwt.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.RefreshToken;
@Service
public interface RefreshTokenService {

	public Optional<RefreshToken> findByToken(String token);

	public RefreshToken createRefreshToken(Long id);

	public RefreshToken verifyExpiration(RefreshToken token);

	public Boolean deleteByUserId(Long id);
}
