package com.talevski.jwt.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.User;
import com.talevski.jwt.model.domain.Like;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.domain.UserDto;
import com.talevski.jwt.model.dto.PostDto;
import com.talevski.jwt.model.mapper.PostMapper;
import com.talevski.jwt.repository.LikeRepository;
import com.talevski.jwt.repository.PostRepository;
import com.talevski.jwt.repository.UserRepository;
import com.talevski.jwt.service.LikeService;
@Service
public class LikeServiceImpl implements LikeService{
	
	@Autowired
	private LikeRepository likeRepository;
	
	@Autowired
	private PostRepository postRepository;
	
	@Autowired
	private PostMapper postMapper;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Like findById(Integer id) {
		return likeRepository.findById(id).orElseThrow(()-> {
			return new ApiException("not found");
		});
	}

	@Override
	public Like likeIt(Integer id, Like entity) {
		Post persistedPost = postRepository.findById(id).orElseThrow(()-> {
			return new ApiException("Post not found");
		});
		persistedPost.getLikes().stream().forEach((x) -> {
			if(x.getUserId().equals(entity.getUserId()))
				throw new ApiException("You cant like the same post twice..");
		});
		Like like = new Like();
		like.setPost(persistedPost);
		like.setUserId(entity.getUserId());
		PostDto transientPost = postMapper.entityToDto(persistedPost);
		transientPost.getLikes().add(like);
		postMapper.mapRequestedFieldForUpdate(transientPost, persistedPost);
		postRepository.saveAndFlush(persistedPost);
		likeRepository.save(like);
		return like;
	}

	@Override
	public List<Like> getAll() {
		return likeRepository.findAll();
	}

	@Override
	public void removeLike(Integer id) {
		likeRepository.deleteById(id);
		
	}

	@Override
	public List<UserDto> likeUsers(Integer id) {
		Post post = postRepository.findById(id).orElseThrow(()-> {
			return new ApiException("Post not found");
		});
		List<UserDto> users = new ArrayList<UserDto>();
		post.getLikes().stream().forEach(x -> {
			User user = userRepository.findById((long) x.getUserId()).get();
			UserDto userDto = new UserDto();
			userDto.setId(user.getId().intValue());
			userDto.setUsername(user.getUsername());
			userDto.setEmail(user.getEmail());
			users.add(userDto);
		});
		
		return users;
	}

}
