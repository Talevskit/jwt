package com.talevski.jwt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.domain.AddComment;
import com.talevski.jwt.model.domain.Comment;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.repository.CommentRepository;
import com.talevski.jwt.repository.PostRepository;
import com.talevski.jwt.service.CommentService;
@Service
public class CommentServiceImpl implements CommentService{
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private PostRepository postRepository;

	@Override
	public Comment findById(Integer id) {
		return commentRepository.findById(id).orElseThrow(() -> {
			return new ApiException("Comment not found.");
		});
	}

	@Override
	public Comment addComment(AddComment addComment) {
		Comment comment = new Comment();
		comment.setUsername(addComment.getComment().getUsername());
		comment.setComment(addComment.getComment().getComment());
		Post post = postRepository.findById(addComment.getId()).get();
		comment.setPost(post);
		commentRepository.saveAndFlush(comment);
		post.getComments().add(comment);
		postRepository.save(post);
		return comment;
	}

	@Override
	public Comment updateComment(Integer id, Comment comment) {
		Comment updated = commentRepository.findById(id).orElseThrow(() -> {
			return new ApiException("Comment not found");
		});
		updated.setUsername(comment.getUsername());
		updated.setComment(comment.getComment());
		updated.setPost(postRepository.findById(comment.getPost().getId()).get());
		return commentRepository.save(updated);
	}

	@Override
	public List<Comment> getAll() {
		return commentRepository.findAll();
	}

	@Override
	public void removeComment(Integer id) {
		commentRepository.deleteById(id);
		
	}
	
	

}
