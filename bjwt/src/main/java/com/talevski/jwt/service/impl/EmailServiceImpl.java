package com.talevski.jwt.service.impl;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.talevski.jwt.model.EmailObj;
import com.talevski.jwt.model.ResetPasswordObject;
import com.talevski.jwt.model.User;
import com.talevski.jwt.payload.request.EmailRequest;
import com.talevski.jwt.repository.ResetPasswordRepository;
import com.talevski.jwt.repository.UserRepository;

@Service
public class EmailServiceImpl {

	@Autowired
	private SendGrid sendGrid;

	@Autowired
	private ResetPasswordRepository resetPasswordRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder encoder;

	public Response sendEmail(EmailRequest emailRequest) {
		Mail mail = new Mail(new Email("clubs.operator@gmail.com"), emailRequest.getSubject(),
				new Email(emailRequest.getTo()), new Content("text/plain", emailRequest.getBody()));
		mail.setReplyTo(new Email("clubs.operator@gmail.com"));
		Request request = new Request();
		Response response = null;
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			response = this.sendGrid.api(request);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
		return response;
	}

	public String resetPassword(EmailObj emailObj) {
		Optional<User> user = userRepository.findByEmail(emailObj.getEmail());

		ResetPasswordObject resetPasswordObject = new ResetPasswordObject();
		resetPasswordObject.setUserId(user.get().getId());
		resetPasswordObject.setToken(UUID.randomUUID().toString());
		resetPasswordRepository.save(resetPasswordObject);

		String appUrl = "http://localhost:8080/api/log/resetPassword?token=";
		EmailRequest email = new EmailRequest(emailObj.getEmail(), "Reset Password",
				"To reset your password, click the link below: \n" + appUrl + "" + resetPasswordObject.getToken());
			sendEmail(email);
		return appUrl + "" + resetPasswordObject.getToken();
	}

	public String setNewPassword(String token, String password) throws Exception {
		ResetPasswordObject resetToken = resetPasswordRepository.findByToken(token);
		Optional<User> user = userRepository.findById(resetToken.getUserId());
		if (user.isPresent()) {
			User resetUser = user.get();
			resetUser.setPassword(encoder.encode(password));
			userRepository.save(resetUser);
			resetPasswordRepository.delete(resetToken);
		} else {
			throw new Exception("Member Not Found");
		}
		return "Password Changed";
	}

}
