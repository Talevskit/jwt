package com.talevski.jwt.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.domain.AddComment;
import com.talevski.jwt.model.domain.Comment;
import com.talevski.jwt.model.domain.Post;

@Service
public interface CommentService {
	
	public Comment findById(Integer id);

	public Comment addComment(AddComment addComment);

	public Comment updateComment(Integer id, Comment comment);

	public List<Comment> getAll();
	
	public void removeComment(Integer id);

}
