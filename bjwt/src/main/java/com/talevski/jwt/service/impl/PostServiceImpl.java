package com.talevski.jwt.service.impl;
import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.domain.Dislike;
import com.talevski.jwt.model.domain.Like;
import com.talevski.jwt.model.domain.Post;
import com.talevski.jwt.model.dto.PostDto;
import com.talevski.jwt.model.mapper.PostMapper;
import com.talevski.jwt.repository.DislikeRepository;
import com.talevski.jwt.repository.LikeRepository;
import com.talevski.jwt.repository.PostRepository;
import com.talevski.jwt.service.PostService;
@Service
public class PostServiceImpl implements PostService{
	private static final Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
	
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private PostMapper postMapper;

	@Override
	public PostDto findById(Integer id) {
		Post postEntity = postRepository.findById(id).orElseThrow(() -> {
			logger.debug("Post not found!");
			return new ApiException("Post not found.");
		});
		return postMapper.entityToDto(postEntity);
	}

	@Override
	public PostDto createPost(PostDto postDto) {
		Post transientPost = postMapper.dtoToEntity(postDto);
		Post persistedPost = postRepository.save(transientPost);
		return postMapper.entityToDto(persistedPost);
	}

	@Override
	public PostDto updatePost(Integer id, PostDto postDto) {
		Post persistedPost = postRepository.findById(id).get();
		postMapper.mapRequestedFieldForUpdate(postDto, persistedPost);
		return postMapper.entityToDto(postRepository.saveAndFlush(persistedPost));
	}
	


	@Override
	public List<PostDto> getAll() {
		return postMapper.mapList(postRepository.findAll(), PostDto.class);
	}

	@Override
	public void removePost(Integer id) {
		postRepository.deleteById(id);
	}
	
}
