package com.talevski.jwt.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.domain.AddComment;
import com.talevski.jwt.model.dto.PostDto;

@Service
public interface PostService {
	
	public PostDto findById(Integer id);

	public PostDto createPost(PostDto postDto);

	public PostDto updatePost(Integer id, PostDto postDto);

	public List<PostDto> getAll();
	
	public void removePost(Integer id);
	
//	public PostDto likePost(Integer id);
//	
//	public PostDto dislikePost(Integer id);
	
}
