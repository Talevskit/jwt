package com.talevski.jwt.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.domain.Dislike;
import com.talevski.jwt.model.domain.UserDto;
import com.talevski.jwt.model.dto.PostDto;
@Service
public interface DislikeService {
	
	public Dislike findById(Integer id);

	public Dislike dislikeIt(Integer id, Dislike dislike);

	public List<Dislike> getAll();
	
	public void removeDislike(Integer id);
	
	public List<UserDto> dislikeUsers(Integer id);

}
