package com.talevski.jwt.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.talevski.jwt.infrastructure.exception.ApiException;
import com.talevski.jwt.model.TokenEmailObj;
import com.talevski.jwt.model.User;
import com.talevski.jwt.repository.UserRepository;
import com.talevski.jwt.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder encoder;

	@Override
	public User findById(Long id) {
		return userRepository.findById(id).orElseThrow(() -> {
			return new ApiException("User not found");
		});
	}

	@Override
	public List<User> getAll() {
		return userRepository.findAll();
	}


	@Override
	public Boolean removeUser(Long id) {
		userRepository.deleteById(id);
		return true;
	}
	
	public void sendEmailToResetPassword(TokenEmailObj tokenEmailObj) {
		User user = userRepository.findByEmail(tokenEmailObj.getEmail()).orElseThrow(()-> {
			return new ApiException("User not found.");
		});
		user.setResetPasswordToken(tokenEmailObj.getToken());
		userRepository.save(user);
	}
	
	public User get(String resetPasswordToken) {
		return userRepository.findByResetPasswordToken(resetPasswordToken).orElseThrow(()-> {
			return new ApiException("User not found.");
		});
	}
	
	public void updatePassword(User user, String newPassword) {
		String encodedPassword = encoder.encode(newPassword);
		user.setPassword(encodedPassword);
		user.setResetPasswordToken(null);
		userRepository.save(user);
	}

}
