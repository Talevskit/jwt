package com.talevski.jwt.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.talevski.jwt.model.domain.Like;
import com.talevski.jwt.model.domain.UserDto;
import com.talevski.jwt.model.dto.PostDto;

@Service
public interface LikeService {

	public Like findById(Integer id);

	public Like likeIt(Integer id, Like like);

	public List<Like> getAll();
	
	public void removeLike(Integer id);
	
	public List<UserDto> likeUsers(Integer id);
	}
