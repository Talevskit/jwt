package com.talevski.jwt.infrastructure.domain;

import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;

import lombok.Data;
@Data
@MappedSuperclass
public class BasePost {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Date createdOn;
	
	@PrePersist
	public void init() {
		createdOn = Date.from(java.time.ZonedDateTime.now(ZoneOffset.UTC).toInstant());
	}

}
