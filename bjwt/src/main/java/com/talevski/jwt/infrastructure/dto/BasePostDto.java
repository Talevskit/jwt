package com.talevski.jwt.infrastructure.dto;

import java.util.Date;

import lombok.Data;
@Data
public class BasePostDto {
	
	private Integer id;
	private Date createdOn;

}
