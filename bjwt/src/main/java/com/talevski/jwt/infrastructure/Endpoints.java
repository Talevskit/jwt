package com.talevski.jwt.infrastructure;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Endpoints {
	public static final String BASE = "/api";
	public static final String POSTS = BASE + "/posts";
	public static final String AUTH = BASE + "/log";
	public static final String TEST = BASE + "/test";
	
}
