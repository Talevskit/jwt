package com.talevski.jwt.payload.request;

import lombok.Getter;

@Getter
public class LogoutRequest {
	
	private Long userId;

}
