package com.talevski.jwt.payload.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefreshTokenRequest {
	
	private String refreshToken;

}
