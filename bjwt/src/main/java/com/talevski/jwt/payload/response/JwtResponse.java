package com.talevski.jwt.payload.response;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponse {
	
	private Long id;
	private String token;
	private String refreshToken;
	private String type = "Bearer";
	private String username;
	private String email;
	private List<String> roles;
	
	
	public JwtResponse(Long id,String token, String refreshToken, String username, String email, List<String> roles) {
		this.id = id;
		this.token = token;
		this.refreshToken = refreshToken;
		this.username = username;
		this.email = email;
		this.roles = roles;
	}

}
