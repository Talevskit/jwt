import "./App.css";
import Signin from "./components/authorization/Signin";
import Signup from "./components/authorization/Signup";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "./components/headerFooter/Header";
import Welcome from "./components/Welcome";
import AdminContent from "./components/roleContents/AdminContent";
import ModeratorContent from "./components/roleContents/ModeratorContent";
import UserContent from "./components/roleContents/UserContent";
import PostList from "./components/posts/PostList";
import ResetPasswordRequest from "./components/authorization/ResetPasswordRequest";
import ReactGA from 'react-ga';
import { useEffect } from "react";


const App = () => {

  const TRACKING_ID = "UA-12341234-1"; // YOUR_OWN_TRACKING_ID
  ReactGA.initialize(TRACKING_ID);

  useEffect(() => {
    ReactGA.event({
      category: 'User',
      action: 'Created an Account'
    });
  }, []);

  
  return (
    <div className="App">
      <Router>
        <Header />
        <div className="container">
          <Switch>
            <Route path="/welcome" component={Welcome}></Route>
            <Route path="/signin" component={Signin}></Route>
            <Route path="/signup" component={Signup}></Route>
            <Route path="/admin" component={AdminContent}></Route>
            <Route path="/moderator" component={ModeratorContent}></Route>
            <Route path="/user" component={UserContent}></Route>
            <Route path="/posts" component={PostList}></Route>
            <Route
              path="/forgotPassword"
              component={ResetPasswordRequest}
            ></Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
};

export default App;
