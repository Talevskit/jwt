import PostList from "../posts/PostList";

const UserContent = () => {
  return (
    <div>
      <h1>User Content</h1>
      <PostList />
    </div>
  );
};

export default UserContent;
