import PostList from "../posts/PostList";

const ModeratorContent = () => {
  return (
    <div>
      <h1>Moderator Content</h1>
      <PostList />
    </div>
  );
};

export default ModeratorContent;
