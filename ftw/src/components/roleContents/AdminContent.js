import PostList from "../posts/PostList";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addPost } from "../../actions/postActions";

const AdminContent = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    let post = {
      title: title,
      description: description,
    };
    dispatch(addPost(post, localStorage.getItem("token")));
  };
  return (
    <div className="row">
      <div className="frm container p-3 my-3 col-4">
        <form onSubmit={handleSubmit}>
          <input
            onChange={(e) => setTitle(e.target.value)}
            type="text"
            className="form-control mb-4"
            placeholder="Title"
          />
          <textarea
            style={{ wordWrap: "breakWord" }}
            onChange={(e) => setDescription(e.target.value)}
            type="password"
            className="form-control mb-4"
            placeholder="Description"
          />
          <button type="submit" className="btn btn-primary btn-lg btn-block">
            Create post
          </button>
        </form>
      </div>
      <div className="col-8">
        <PostList />
      </div>
    </div>
  );
};

export default AdminContent;
