import { useDispatch } from "react-redux";
import {
  adminContent,
  logout,
  moderatorContent,
  userContent,
} from "../actions/userActions";
import { useHistory } from "react-router-dom";

const Welcome = () => {
  let history = useHistory();
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Welcome</h1>
      <div className="col mt-4">
        <div className="row">
          <button
            className="btn btn-primary btn-lg btn-block mt-4"
            onClick={() =>
              dispatch(adminContent(localStorage.getItem("token"), history))
            }
          >
            Admin Content
          </button>
        </div>
        <div className="row">
          <button
            className="btn btn-primary btn-lg btn-block mt-4"
            onClick={() =>
              dispatch(moderatorContent(localStorage.getItem("token"), history))
            }
          >
            Moderator Content
          </button>
        </div>
        <div className="row">
          <button
            className="btn btn-primary btn-lg btn-block mt-4"
            onClick={() =>
              dispatch(userContent(localStorage.getItem("token"), history))
            }
          >
            User Content
          </button>
        </div>
        <div className="row">
          <button
            className="btn btn-primary btn-lg btn-block mt-4"
            onClick={() => console.log(localStorage.getItem("token"))}
          >
            getJwt
          </button>
        </div>
        <div className="row">
          <button
            className="btn btn-danger btn-lg btn-block mt-4"
            onClick={() => dispatch(logout(history))}
          >
            Logout
          </button>
        </div>
      </div>
    </div>
  );
};

export default Welcome;
