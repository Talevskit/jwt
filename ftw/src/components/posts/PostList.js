import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import { useSelector, useDispatch } from "react-redux";
import { CgClose } from 'react-icons/cg';
import {
  dislikePost,
  getPosts,
  likePost,
  addComment,
  deletePost,
  deleteComment,
  getLikes,
  getDislikes,
} from "../../actions/postActions";

const PostList = () => {
  const [comment, setComment] = useState("");
  const [openLikes, setOpenLikes] = useState(false);
  const [openDislikes, setOpenDislikes] = useState(false);
  let jwt = localStorage.getItem("token");
  let username = localStorage.getItem("username");
  let id = localStorage.getItem("id");
  const dispatch = useDispatch();
  const posts = useSelector((state) => state.posts.posts);
  const liked = useSelector((state) => state.posts.likedPost);
  const disliked = useSelector((state) => state.posts.dislikedPost);

  useEffect(() => {
    dispatch(getPosts(jwt));
  }, []);
  const modalStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      border: "1px solid black",
      height: "600px",
      width: "300px",
    backgroundColor: "#f2f2f2"
    }
  };

  const modalItemBorder = {
    marginTop: "40px",
    borderBottom: "1px solid black",
  }

  const addCmnt = (postId) => (e) => {
    e.preventDefault();
    let commentObj = {
      id: postId,
      comment: {
        username: username,
        comment: comment,
      },
    };
    dispatch(addComment(commentObj, jwt));
    setComment("");
  };
  const handleDeletePost = (id) => {
    dispatch(deletePost(id, jwt));
  };
  const handleDeleteComment = (postId, commentId) => {
    dispatch(deleteComment(postId, commentId, jwt));
  };

  const handleLike = (post) => {
    let like = {
      userId: id,
    };
    dispatch(likePost(post.id, like, jwt));
  };

  const handleDislike = (post) => {
    let dislike = {
      userId: id,
    };
    dispatch(dislikePost(post.id, dislike, jwt));
  };

  const handleGetLikes = (id) => {
    setOpenLikes(true);
    dispatch(getLikes(id, jwt));
  };
  const handleGetDislikes = (id) => {
    setOpenDislikes(true);
    dispatch(getDislikes(id, jwt));
  };

  return (
    <div className="container">
      {/* <div>{posts ?? <div className="spinner-grow text-primary"></div> }</div> */}
      {posts.map((post) => (
        <div key={post.id} className="p-2 border border-secondary m-2 post">
          <div className="row">
            <div className="col-10">
              <h2 className="row pl-4">{post.title}</h2>
              <p className="row pl-4">{post.description}</p>
            </div>
            {localStorage.getItem("role") === "ROLE_MODERATOR" ? (
              <div className="col-2">
                <button
                  className="btn btn-secondary p-2 mt-2"
                  onClick={() => handleDeletePost(post.id)}
                >
                  Delete Post
                </button>
              </div>
            ) : (
              ""
            )}
          </div>
          <hr />
          <div className="row reacts">
            <button
              className="btn btn-success m-2"
              onClick={() => handleLike(post)}
            >
              Like
            </button>

            <p className="mt-3 pointer" onClick={() => handleGetLikes(post.id)}>
              {post.likes.length > 0 ? post.likes.length : ""}{" "}
              {post.likes.length === 0
                ? ""
                : post.likes.length === 1
                ? "like"
                : "likes"}
            </p>
            <Modal
              isOpen={openLikes}
              style={modalStyles}
            >
              <button
                className="btn btn-secondary p-2 m-2"
                style={{position: "absolute",
                        top: "5px",
                        right: "5px"}}
                onClick={() => setOpenLikes(false)}
              >
                  <CgClose />
              </button>
              
              {Modal.setAppElement("#root")}
              {liked &&
                liked.map((user) => (
                  <div key={user.id} style={modalItemBorder}>
                    <p>
                      <strong>
                        {user.username} - {user.email}
                      </strong>
                    </p>
                  </div>
                ))}
                
            </Modal>
            <button
              className="btn btn-danger m-2"
              onClick={() => handleDislike(post)}
            >
              Dislike
            </button>
            <p
              className="mt-3 pointer"
              onClick={() => handleGetDislikes(post.id, jwt)}
            >
              {post.dislikes.length > 0 ? post.dislikes.length : ""}{" "}
              {post.dislikes.length === 0
                ? ""
                : post.dislikes.length === 1
                ? "dislike"
                : "dislikes"}
            </p>
            <Modal
              isOpen={openDislikes}
              style={modalStyles}
            >
              <button
                className="btn btn-secondary p-2 m-2"
                style={{position: "absolute",
                        top: "5px",
                        right: "5px"}}
                onClick={() => setOpenDislikes(false)}
              >
                  <CgClose />
              </button>
              {Modal.setAppElement("#root")}
              {disliked.map((user) => (
                <div key={user.id} style={modalItemBorder}>
                  <p>
                    <strong>
                      {user.username} - {user.email}
                    </strong>
                  </p>
                </div>
              ))}
            </Modal>
          </div>
          <div>
            {post.comments &&
              post.comments.map((comment) => (
                <div key={comment.id} className="comment">
                  <p>
                    <strong>{comment.username}</strong>
                    {localStorage.getItem("username") === comment.username ? (
                      <button
                        className=" btn btn-danger btn-sm m-2 float-right circularBtn"
                        onClick={() => handleDeleteComment(post.id, comment.id)}
                      >
                        <CgClose />
                        
                      </button>
                      
                    ) : (
                      ""
                    )}
                    <br />
                    {comment.comment}
                  </p>
                </div>
              ))}
            <div>
              <form onSubmit={addCmnt(post.id)}>
                <textarea
                  id="commentInput"
                  style={{ wordWrap: "breakWord" }}
                  type="text"
                  className="form-control mb-3 p-4"
                  placeholder="Add a comment.."
                  onChange={(e) => setComment(e.target.value)}
                />
                <button type="submit" className=" btn btn-info btn-block">
                  Post
                </button>
              </form>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default PostList;
