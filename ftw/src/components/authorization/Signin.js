import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { loginUser } from "../../actions/userActions";
import { useHistory } from "react-router-dom";

const Signin = () => {
  let history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    let user = {
      username: username,
      password: password,
    };
    dispatch(loginUser(user, history));
  };
  return (
    <div className="frm container p-3 my-3 col-6">
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setUsername(e.target.value)}
          type="text"
          className="form-control mb-4"
          placeholder="Username"
        />
        <input
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          className="form-control mb-4"
          placeholder="Password"
        />
        <button type="submit" className="btn btn-primary btn-lg btn-block mb-2">
          Login
        </button>
        <a href="/forgotPassword">Forgot password?</a>
      </form>
    </div>
  );
};

export default Signin;
