import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { registerUser } from "../../actions/userActions";
import { useHistory } from "react-router-dom";

const Signup = () => {
  let history = useHistory();

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState([]);
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    let user = {
      username: username,
      email: email,
      role: [role.toString().toLowerCase()],
      password: password,
    };
    dispatch(registerUser(user, history));
    console.log(username);
    console.log(email);
    console.log([role.toString().toLowerCase()]);
    console.log(password);
  };
  return (
    <div className="frm container p-3 my-3 col-6">
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setUsername(e.target.value)}
          type="text"
          className="form-control mb-4"
          placeholder="Username"
        />
        <input
          onChange={(e) => setEmail(e.target.value)}
          type="text"
          className="form-control mb-4"
          placeholder="Email"
        />
        <select
          className="form-control mb-4"
          onChange={(e) => {
            setRole(
              e.target.value === "Moderator" ? ["mod"] : [e.target.value]
            );
          }}
        >
          <option selected disabled hidden>
            Select role
          </option>
          <option>Admin</option>
          <option>Moderator</option>
          <option>User</option>
        </select>
        <input
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          className="form-control mb-4"
          placeholder="Password"
        />
        <button type="submit" className="btn btn-primary btn-lg btn-block">
          Register
        </button>
      </form>
    </div>
  );
};

export default Signup;
