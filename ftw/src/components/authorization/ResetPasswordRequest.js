import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { resetPasswordRequest } from "../../actions/userActions";

const ResetPasswordRequest = () => {
  const [email, setEmail] = useState("");
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    let emailObj = {
      email: email,
    };
    dispatch(resetPasswordRequest(emailObj));
  };

  return (
    <div className="frm container p-3 my-3 col-6">
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setEmail(e.target.value)}
          type="text"
          className="form-control mb-4"
          placeholder="Email"
        />
        <button type="submit" className="btn btn-primary btn-lg btn-block">
          Send
        </button>
      </form>
    </div>
  );
};

export default ResetPasswordRequest;
