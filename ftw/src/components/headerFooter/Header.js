import { useHistory } from "react-router-dom";

const Header = () => {
  let history = useHistory();

  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <div>
            <a href="/#" className="navbar-brand" onClick={() => history.push("/")}>
              MAME MI
            </a>
            <div style={{ position: "absolute", top: 10, right: 10 }}>
              <button
                className="btn btn-outline-danger ml-3"
                onClick={() => history.push("/signin")}
              >
                Sign in
              </button>
              <button
                className="btn btn-outline-danger ml-3"
                onClick={() => history.push("/signup")}
              >
                Sign up
              </button>
            </div>
          </div>
        </nav>
      </header>
    </div>
  );
};
export default Header;
