export const ADMIN_CONTENT = "ADMIN_CONTENT";
export const MODERATOR_CONTENT = "MODERATOR_CONTENT";
export const USER_CONTENT = "USER_CONTENT";

export const USER_LOADING = "USER_LOADING";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";
export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";

export const GET_POSTS = "GET_POSTS";
export const GET_POST = "GET_POST";
export const DELETE_POST = "DELETE_POST";
export const DELETE_COMMENT = "DELETE_COMMENT";
export const ADD_POST = "ADD_POST";
export const UPDATE_POST = "UPDATE_POST";
export const ADD_COMMENT = "ADD_COMMENT";
export const LIKE_POST = "LIKE_POST";
export const DISLIKE_POST = "DISLIKE_POST";
export const GET_LIKES = "GET_LIKES";
export const GET_DISLIKES = "GET_DISLIKES";

export const RESET_PASSWORD = "RESET_PASSWORD";
