import axios from "../service/Service";
import { returnErrors } from "./errorActions";
import {
  GET_POSTS,
  GET_POST,
  DELETE_POST,
  DELETE_COMMENT,
  ADD_POST,
  UPDATE_POST,
  ADD_COMMENT,
  LIKE_POST,
  DISLIKE_POST,
  GET_LIKES,
  GET_DISLIKES,
} from "./types";

export const getPosts = (jwt) => (dispatch) => {
  return axios
    .get("/posts", {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: GET_POSTS,
        payload: data,
      });
      console.log(data);
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const getPost = (id, jwt) => (dispatch) => {
  return axios
    .get(`/posts/${id}`, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: GET_POST,
        payload: data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const addPost = (post, jwt) => (dispatch) => {
  return axios
    .post("/posts", post, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: ADD_POST,
        payload: data,
      });
      console.log(data);
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const addComment = (commentObj, jwt) => (dispatch) => {

  return axios
    .put(`/posts/addComment`, commentObj, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      let obj = {
        postId: commentObj.id,
        comment: data
      }
      dispatch({
        type: ADD_COMMENT,
        payload: obj,
      });
      console.log(123,obj)
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });

};

export const deletePost = (id, jwt) => (dispatch) => {
  return axios
    .delete(`/posts/${id}`, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then((res) => {
      dispatch({
        type: DELETE_POST,
        payload: id,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};
export const deleteComment = (postId, commentId, jwt) => (dispatch) => {
  return axios
    .delete(`/posts/deleteComment/${commentId}`, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then((res) => {
      let commentInPost = {
        postId: postId,
        commentId: commentId
      }
      dispatch({
        type: DELETE_COMMENT,
        payload: commentInPost,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const updatePost = (id, post, jwt) => (dispatch) => {
  return axios
    .put(`/posts/${id}`, post, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: UPDATE_POST,
        payload: data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const likePost = (id, like, jwt) => (dispatch) => {
  return axios
    .post(`/posts/like/${id}`, like, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      let obj = { postId: id, like: data }
      console.log(obj);
      dispatch({
        type: LIKE_POST,
        payload: obj
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const dislikePost = (id, dislike, jwt) => (dispatch) => {
  return axios
    .post(`/posts/dislike/${id}`, dislike, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      let obj = { postId: id, dislike: data }
      console.log(obj);
      dispatch({
        type: DISLIKE_POST,
        payload: obj
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const getLikes = (id, jwt) => (dispatch) => {
  return axios
    .get(`/posts/getLikes/${id}`, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: GET_LIKES,
        payload: data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const getDislikes = (id, jwt) => (dispatch) => {
  return axios
    .get(`/posts/getDislikes/${id}`, {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then(({ data }) => {
      dispatch({
        type: GET_DISLIKES,
        payload: data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};
