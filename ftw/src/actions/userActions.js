import axios from "../service/Service";

import {
  ADMIN_CONTENT,
  MODERATOR_CONTENT,
  USER_CONTENT,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  RESET_PASSWORD,
} from "./types";
import { returnErrors } from "./errorActions";

export const loginUser = (user, history) => (dispatch) => {
  return axios
    .post("/log/signin", user)
    .then((res) => {
      console.log("Logged in succesfully");
      history.push("/welcome");
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
      dispatch({
        type: LOGIN_FAIL,
      });
    });
};

export const registerUser = (user, history) => (dispatch) => {
  return axios
    .post("/log/signup", user)
    .then((res) => {
      console.log("User registered succesfully");
      history.push("/signin");
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
      dispatch({
        type: REGISTER_FAIL,
      });
    });
};

export const logout = (history) => {
  history.push("/signin");
  return {
    type: LOGOUT_SUCCESS,
  };
};

export const adminContent = (jwt, history) => (dispatch) => {
  return axios
    .get("/test/admin", {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then((res) => {
      console.log("go to admin content");
      history.push("/admin");
      dispatch({
        type: ADMIN_CONTENT,
        payload: jwt,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const moderatorContent = (jwt, history) => (dispatch) => {
  return axios
    .get("/test/mod", {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then((res) => {
      console.log("go to moderator content");
      history.push("/moderator");
      dispatch({
        type: MODERATOR_CONTENT,
        payload: jwt,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const userContent = (jwt, history) => (dispatch) => {
  return axios
    .get("/test/user", {
      headers: {
        Authorization: "Bearer " + jwt,
      },
    })
    .then((res) => {
      console.log("go to user content");
      history.push("/user");
      dispatch({
        type: USER_CONTENT,
        payload: jwt,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};

export const resetPasswordRequest = (email) => (dispatch) => {
  return axios
    .post(`/log/resetPasswordRequest`, email)
    .then(({ data }) => {
      dispatch({
        type: RESET_PASSWORD,
        payload: data,
      });
    })
    .catch((err) => {
      dispatch(returnErrors(err.message, err.status));
    });
};
