import {
  GET_POSTS,
  GET_POST,
  DELETE_POST,
  DELETE_COMMENT,
  ADD_POST,
  UPDATE_POST,
  ADD_COMMENT,
  LIKE_POST,
  DISLIKE_POST,
  GET_LIKES,
  GET_DISLIKES,
} from "../actions/types";

const initialState = {
  posts: [
    // {
    //   title: null,
    //   description: null,
    //   comments: [],
    //   likes: [],
    //   dislikes: [],
    // },
  ],
  likedPost: [],
  dislikedPost: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        posts: action.payload,
      };
    case GET_POST:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      };
    case ADD_POST:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      };
    case ADD_COMMENT:
      return {
        ...state,
        posts: state.posts.map(post => {
          if(post.id === action.payload.postId) {
            return {
              ...post,
              comments: [...post.comments, action.payload.comment]
          }
        }else return post})
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter((stud) => stud.id !== action.payload),
      };
    case DELETE_COMMENT:
      return {
        ...state,
        posts: state.posts.map(post => {
          if(post.id === action.payload.postId) {
            return {
              ...post,
              comments: post.comments.filter(comment => 
                comment.id !== action.payload.commentId
              )
          }
        }else return post})
      }
    case UPDATE_POST:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      };
    case LIKE_POST:
       return {
         ...state,
         posts: state.posts.map(post => {
           if(post.id !== action.payload.postId){
             return post
              } else {
                return {
                 ...post,
                 likes: [...post.likes, action.payload.like]
               }
             }
         })
       }
    case DISLIKE_POST:
      return {
        ...state,
        posts: state.posts.map(post => {
          if(post.id !== action.payload.postId){
            return post
             } else {
               return {
                ...post,
                dislikes: [...post.dislikes, action.payload.dislike]
              }
            }
        })
      }
    case GET_LIKES:
      return {
        ...state,
        likedPost: action.payload,
      };
    case GET_DISLIKES:
      return {
        ...state,
        dislikedPost: action.payload,
      };
    default:
      return state;
  }
};
