import { combineReducers } from "redux";
import errorReducer from "./errorReducer";
import postReducer from "./postReducer";
import userReducer from "./userReducer";

export default combineReducers({
  users: userReducer,
  error: errorReducer,
  posts: postReducer,
});
