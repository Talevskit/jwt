import {
  ADMIN_CONTENT,
  MODERATOR_CONTENT,
  USER_CONTENT,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  RESET_PASSWORD,
} from "../actions/types";

const initialState = {
  // token: localStorage.getItem("token"),
  isAuthenticated: null,
  isLoading: false,
  user: null,
  resetPasswordToken: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      localStorage.setItem("token", action.payload.token);
      localStorage.setItem("username", action.payload.username);
      localStorage.setItem("role", action.payload.roles);
      localStorage.setItem("id", action.payload.id);
      console.log(action.payload);
      return {
        ...state,
        user: action.payload,
        isAuthenticated: true,
        isLoading: false,
      };
    case LOGIN_FAIL:
    case LOGOUT_SUCCESS:
    case REGISTER_FAIL:
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      localStorage.removeItem("role");
      localStorage.removeItem("id");
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        isLoading: false,
      };
    case ADMIN_CONTENT:
      return {
        ...state,
      };
    case MODERATOR_CONTENT:
      return {
        ...state,
      };
    case USER_CONTENT:
      return {
        ...state,
      };
    case RESET_PASSWORD:
      return {
        ...state,
        resetPasswordToken: action.payload,
      };

    default:
      return state;
  }
};
